import {Component, OnInit, ViewChild} from '@angular/core';
import {NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthService} from "../_services/auth.service";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  retUrl: string = "register";
  @ViewChild('f', {static: false}) form: NgForm;

  constructor(private authService: AuthService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }

  ngOnInit() {
    this.activatedRoute.queryParamMap
      .subscribe(params => {
        this.retUrl = params.get('returnUrl');
        console.log('LoginComponent/ngOnInit ' + params.get('returnUrl'));
      });
  }

  onSubmit(form: NgForm): void {
    // console.log(form.value.inputEmail);
    // console.log(form.value.inputPassword);
    // console.log(form.value.rememberMe);
    this.authService.login(form.value.inputEmail, form.value.inputPassword)
      .subscribe(data => {
        console.log('return to ' + this.retUrl);
        if (this.retUrl != null) {
          this.router.navigate([this.retUrl]);
        } else {
          this.router.navigate(['register']);
        }
      });
  }

}
